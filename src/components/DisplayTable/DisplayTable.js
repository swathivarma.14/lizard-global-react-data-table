import React from 'react';
import './DisplayTable.scss';
import { Layout, Table } from 'antd';
import data from '../../mock/data.json';

const { Header } = Layout;

const getDate = (date) => {
  const dateAndTime = date.split('T');
  return dateAndTime[0].split('-').reverse().join('-');
}

const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
      width: '10%',
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
      width: '10%',
    },
    {
      title: 'Author',
      dataIndex: 'author',
      key: 'author',
      width: '7%',
      render: (item) => Object.values(item)[0],
    },
    {
      title: 'Summary',
      dataIndex: 'summary',
      key: 'summary',
      width: '15%',
    },
    {
      title: 'Publish Date',
      dataIndex: 'publishDate',
      key: 'publishDate',
      width: '5%',
      render: ((publishDate: string) => getDate(publishDate)),
      sorter: (a, b) => new Date(a.publishDate) - new Date(b.publishDate),
    },
    {
      title: 'Categories',
      dataIndex: 'categories',
      key: 'categories',
      width: '15%',
      render: (item) => item.map(category => category.name).join(),
      filters: [
        {
          text: 'Surveys and Forms',
          value: 'Surveys and Forms',
        },
        {
          text: 'Marketing Analytics',
          value: 'Marketing Analytics',
        },
        {
          text: 'Data Management',
          value: 'Data Management',
        },
        {
          text: 'Landing Pages',
          value: 'Landing Pages',
        },
        {
          text: 'Ecommerce',
          value: 'Ecommerce',
        },
        {
          text: 'Tips and Best Practise',
          value: 'Tips and Best Practise',
        },        
        {
          text: 'Marketing Automation',
          value: 'Marketing Automation',
        },
      ],
      onFilter: (value: string, record) => {
        const filteredData = record.categories.map(category => category.name).join();
        return filteredData.includes(value);
      },
    }
];

export default function DisplayTable() {

    return (
        <>
          <div className='App'>
            <Layout>
              <Header>Lizard Global React Developer Assessment</Header>
              <Table
                  dataSource={data.posts}
                  columns={columns}
                  rowClassName={(record, index) => index % 2 === 0 ? 'table-row-light' :  'table-row-dark'}
              />
            </Layout>
          </div>
        </>
    );
}
